package famqp

import (
	"github.com/streadway/amqp"

	fconfig "gitlab.com/Pahan84/framework/config"
)

const (
	root       = "f.amqp"
	ExtRoot    = root + ".ext"
	URL        = root + ".url"
	Host       = root + ".host"
	Port       = root + ".port"
	Vhost      = root + ".vhost"
	Username   = root + ".username"
	Password   = root + ".password"
	TlsEnabled = root + ".tls.enabled"
	CertFile   = root + ".tls.certFile"
	KeyFile    = root + ".tls.keyFile"
	CAFile     = root + ".tls.CAFile"
)

type Config struct {
	URL string
	CFG amqp.Config
}

func init() {
	fconfig.Add(URL, "", "URL")
	fconfig.Add(Host, "localhost", "host")
	fconfig.Add(Port, 5672, "port")
	fconfig.Add(Username, "guest", "define username")
	fconfig.Add(Password, "guest", "define password")
	fconfig.Add(Vhost, "/", "define vhost")
	fconfig.Add(TlsEnabled, false, "Use TLS")
	fconfig.Add(CertFile, "./cert/out/localhost.crt", "Path to the CRT/PEM file.")
	fconfig.Add(KeyFile, "./cert/out/localhost.key", "Path to the private key file.")
	fconfig.Add(CAFile, "./cert/out/blackbox.crt", "Path to the certificate authority (CA).")
}
