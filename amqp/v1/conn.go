package famqp

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"

	"github.com/streadway/amqp"

	flog "gitlab.com/Pahan84/framework/log"
)

type Ext func(context.Context, *amqp.Connection) error

func NewConnection(ctx context.Context, options *Options, exts ...Ext) (*amqp.Connection, error) {
	logger := flog.FromContext(ctx)

	cfg := connConfig(ctx, options)
	conn, err := newClient(ctx, cfg.URL, cfg.CFG)
	if err != nil {
		return nil, err
	}

	for _, ext := range exts {
		if err := ext(ctx, conn); err != nil {
			panic(err)
		}
	}

	logger.Infof("Connected to AMQP server: %s", options.URL)

	return conn, nil
}

func NewDefaultConnection(ctx context.Context, exts ...Ext) (*amqp.Connection, error) {
	l := flog.FromContext(ctx)

	o, err := DefaultOptions()
	if err != nil {
		l.Fatalf(err.Error())
	}

	return NewConnection(ctx, o, exts...)
}

func newClient(ctx context.Context, url string, pc amqp.Config) (client *amqp.Connection, err error) {
	l := flog.FromContext(ctx)

	// Establish connection with provided config.
	client, err = amqp.DialConfig(url, pc)
	if err != nil {
		return nil, err
	}

	l.Infof("Connected to AMQP server: %v", url)

	return client, err
}

func connConfig(ctx context.Context, o *Options) *Config {
	cfg := &Config{}
	uri := amqp.URI{
		Scheme:   "amqp",
		Host:     o.Host,
		Port:     o.Port,
		Username: o.Username,
		Password: o.Password,
		Vhost:    o.Vhost,
	}
	if o.URL != "" {
		uri, _ = amqp.ParseURI(o.URL)
	}

	if o.TLSEnable {
		uri.Scheme = "amqps"
		cfg.CFG = amqp.Config{TLSClientConfig: createTlsConfig(ctx, o)}
	}

	cfg.URL = uri.String()

	return cfg
}

func createTlsConfig(ctx context.Context, o *Options) *tls.Config {
	l := flog.FromContext(ctx)

	// Load the client certificates from disk
	cert, err := tls.LoadX509KeyPair(o.CertFile, o.KeyFile)
	if err != nil {
		l.Fatalf("could not load client key pair: %s", err)
	}
	if o.CAFile != "" {
		// Create a certificate pool from the certificate authority
		certPool := x509.NewCertPool()
		ca, err := ioutil.ReadFile(o.CAFile)
		if err != nil {
			l.Fatalf("could not read ca certificate: %s", err)
		}

		// Append the certificates from the CA
		if ok := certPool.AppendCertsFromPEM(ca); !ok {
			l.Fatalf("failed to append ca certs")
		}

		return &tls.Config{
			ServerName:         o.Host, // NOTE: this is required!
			Certificates:       []tls.Certificate{cert},
			RootCAs:            certPool,
			InsecureSkipVerify: o.InsecureSkipVerify,
		}

	}

	return &tls.Config{
		ServerName:         o.Host, // NOTE: this is required!
		Certificates:       []tls.Certificate{cert},
		InsecureSkipVerify: o.InsecureSkipVerify,
	}
}
