package famqphealth

import (
	"context"
	"errors"

	"github.com/streadway/amqp"
)

type Checker struct {
	conn *amqp.Connection
}

func (c *Checker) Check(ctx context.Context) error {

	var err error

	if c.conn.IsClosed() {
		err = errors.New("Not connected")
	}

	return err
}

func NewChecker(conn *amqp.Connection) *Checker {
	return &Checker{conn: conn}
}
