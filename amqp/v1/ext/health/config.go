package famqphealth

import (
	"gitlab.com/Pahan84/framework/amqp/v1"
	"gitlab.com/Pahan84/framework/config"
)

const (
	root        = famqp.ExtRoot + ".health"
	name        = root + ".name"
	description = root + ".description"
	required    = root + ".required"
	enabled     = root + ".enabled"
)

func init() {
	fconfig.Add(name, "amqp", "health name")
	fconfig.Add(description, "default connection", "define health description")
	fconfig.Add(required, true, "define health description")
	fconfig.Add(enabled, true, "enable/disable health")
}
