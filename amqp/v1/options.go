package famqp

import (
	"crypto/tls"
	"fmt"

	"github.com/lann/builder"

	fconfig "gitlab.com/Pahan84/framework/config"
)

// Options is the common structure used as the options
type Options struct {
	URL                string
	Host               string
	Port               int
	Username           string
	Password           string
	Vhost              string
	TLSEnable          bool
	InsecureSkipVerify bool
	CertFile           string
	KeyFile            string
	CAFile             string `config:"CAFile"`
	tlsConfig          *tls.Config
}
type optionsBuilder builder.Builder

func (b optionsBuilder) URL(value string) optionsBuilder {
	return builder.Set(b, "URL", value).(optionsBuilder)
}

func (b optionsBuilder) Host(value string) optionsBuilder {
	return builder.Set(b, "Host", value).(optionsBuilder)
}

func (b optionsBuilder) Port(value uint16) optionsBuilder {
	if value == 0 {
		value = 5672
	}
	return builder.Set(b, "Port", value).(optionsBuilder)
}

func (b optionsBuilder) Username(value string) optionsBuilder {
	return builder.Set(b, "Username", value).(optionsBuilder)
}

func (b optionsBuilder) Password(value string) optionsBuilder {
	return builder.Set(b, "Password", value).(optionsBuilder)
}

func (b optionsBuilder) Vhost(value string) optionsBuilder {
	return builder.Set(b, "Vhost", value).(optionsBuilder)
}

func (b optionsBuilder) TlsEnable(value bool) optionsBuilder {
	return builder.Set(b, "Enable", value).(optionsBuilder)
}

func (b optionsBuilder) InsecureSkipVerify(value bool) optionsBuilder {
	return builder.Set(b, "InsecureSkipVerify", value).(optionsBuilder)
}

func (b optionsBuilder) CertFile(value string) optionsBuilder {
	return builder.Set(b, "CertFile", value).(optionsBuilder)
}

func (b optionsBuilder) KeyFile(value string) optionsBuilder {
	return builder.Set(b, "KeyFile", value).(optionsBuilder)
}

func (b optionsBuilder) CAFile(value string) optionsBuilder {
	return builder.Set(b, "CAFile", value).(optionsBuilder)
}

func (b optionsBuilder) Build() Options {
	return builder.GetStruct(b).(Options)
}

var OptionsBuilder = builder.Register(optionsBuilder{}, Options{}).(optionsBuilder)

func DefaultOptions() (*Options, error) {
	o := &Options{}

	err := fconfig.UnmarshalWithPath(root, o)
	if err != nil {
		return nil, err
	}
	fmt.Printf("%v", o)
	return o, nil
}
