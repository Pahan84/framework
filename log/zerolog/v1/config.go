package fzerolog

import (
	fconfig "gitlab.com/Pahan84/framework/config"
)

const (
	Formatter = "f.log.zerolog.formatter"
)

func init() {
	fconfig.Add(Formatter, "TEXT", "formatter TEXT/JSON")
}
