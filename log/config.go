package flog

import (
	fconfig "gitlab.com/Pahan84/framework/config"
)

const (
	root           = "f.log"
	ConsoleEnabled = root + ".console.enabled"
	ConsoleLevel   = root + ".console.level"
	fileRoot       = root + ".file"
	FileEnabled    = fileRoot + ".enabled"
	FileLevel      = fileRoot + ".level"
	FilePath       = fileRoot + ".path"
	FileName       = fileRoot + ".name"
	FileMaxSize    = fileRoot + ".maxsize"
	FileCompress   = fileRoot + ".compress"
	FileMaxAge     = fileRoot + ".maxage"
)

func init() {

	fconfig.Add(ConsoleEnabled, true, "enable/disable console logging")
	fconfig.Add(ConsoleLevel, "INFO", "console log level")
	fconfig.Add(FileEnabled, false, "enable/disable file logging")
	fconfig.Add(FileLevel, "INFO", "console log level")
	fconfig.Add(FilePath, "/tmp", "log path")
	fconfig.Add(FileName, "application.l", "log filename")
	fconfig.Add(FileMaxSize, 100, "log file max size (MB)")
	fconfig.Add(FileCompress, true, "log file compress")
	fconfig.Add(FileMaxAge, 28, "log file max age (days)")

}
