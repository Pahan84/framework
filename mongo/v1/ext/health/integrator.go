package fmongohealth

import (
	"context"

	"gitlab.com/Pahan84/framework/health"
	"gitlab.com/Pahan84/framework/log"
	"gitlab.com/Pahan84/framework/mongo/v1"
)

type Integrator struct {
	options *Options
}

func NewIntegrator(options *Options) *Integrator {
	return &Integrator{options: options}
}

func NewDefaultIntegrator() *Integrator {
	o, err := DefaultOptions()
	if err != nil {
		flog.Fatalf(err.Error())
	}

	return NewIntegrator(o)
}

func (i *Integrator) Register(ctx context.Context, conn *fmongo.Conn) error {

	logger := flog.WithTypeOf(*i)

	logger.Trace("integrating mongo with health")

	checker := NewChecker(conn.Client)
	hc := fhealth.NewHealthChecker(i.options.Name, i.options.Description, checker, i.options.Required, i.options.Enabled)
	fhealth.Add(hc)

	logger.Debug("mongo integrated on health with success")

	return nil
}
