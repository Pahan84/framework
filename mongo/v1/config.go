package fmongo

import (
	fconfig "gitlab.com/Pahan84/framework/config"
)

const (
	root     = "f.mongo"
	uri      = root + ".uri"
	authRoot = root + ".auth"
	username = authRoot + ".username"
	password = authRoot + ".password"
	ExtRoot  = root + ".Ext"
)

func init() {

	fconfig.Add(uri, "mongodb://localhost:27017/temp", "define mongodb uri")
	fconfig.Add(username, "", "define mongodb username")
	fconfig.Add(password, "", "define mongodb password")

}
