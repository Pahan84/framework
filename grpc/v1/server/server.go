package fgrpc

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/channelz/service"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/encoding/gzip"
	"google.golang.org/grpc/reflection"

	fconfig "gitlab.com/Pahan84/framework/config"
	flog "gitlab.com/Pahan84/framework/log"
)

var (
	instance *grpc.Server
)

type Ext func(ctx context.Context) []grpc.ServerOption

func New(ctx context.Context, exts ...Ext) (*grpc.Server, grpc.ServiceRegistrar) {

	l := flog.FromContext(ctx)

	err := gzip.SetLevel(9)
	if err != nil {
		l.Fatalf("could not set level: %s", err.Error())
	}

	var s *grpc.Server

	var serverOptions []grpc.ServerOption

	if fconfig.Bool(tlsEnabled) {

		// Load the certificates from disk
		certificate, err := tls.LoadX509KeyPair(fconfig.String(certFile), fconfig.String(keyFile))
		if err != nil {
			l.Fatalf("could not load server key pair: %s", err)
		}

		// Create a certificate pool from the certificate authority
		certPool := x509.NewCertPool()
		ca, err := ioutil.ReadFile(fconfig.String(caFile))
		if err != nil {
			l.Fatalf("could not read ca certificate: %s", err)
		}

		// Append the client certificates from the CA
		if ok := certPool.AppendCertsFromPEM(ca); !ok {
			l.Fatalf("failed to append client certs")
		}

		// Create the TLS credentials
		creds := credentials.NewTLS(&tls.Config{
			ClientAuth:   tls.RequireAndVerifyClientCert,
			Certificates: []tls.Certificate{certificate},
			ClientCAs:    certPool,
		})

		serverOptions = append(serverOptions, grpc.Creds(creds))
	}

	for _, ext := range exts {
		serverOptions = append(serverOptions, ext(ctx)...)
	}

	serverOptions = append(serverOptions, grpc.MaxConcurrentStreams(uint32(fconfig.Int64(maxConcurrentStreams))))

	s = grpc.NewServer(serverOptions...)

	// grpc.InitialConnWindowSize(100),
	// grpc.InitialWindowSize(100),

	instance = s

	return instance, instance
}

func Serve(ctx context.Context) {

	l := flog.FromContext(ctx)

	service.RegisterChannelzServiceToServer(instance)

	// Register reflection service on gRPC server.
	reflection.Register(instance)

	port := fconfig.Int(port)

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		l.Fatalf("failed to listen: %v", err)
		return
	}

	l.Infof("grpc server started on port %v", port)

	if err := instance.Serve(lis); err != nil {
		l.Fatalf("failed to serve: %v", err)
		return
	}

}
