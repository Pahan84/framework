package fgrpcprometheus

import (
	"gitlab.com/Pahan84/framework/config"
	"gitlab.com/Pahan84/framework/grpc/v1/server"
)

const (
	root    = fgrpc.ExtRoot + ".prometheus"
	enabled = root + ".enabled"
)

func init() {
	fconfig.Add(enabled, true, "enable/disable prometheus")
}

func IsEnabled() bool {
	return fconfig.Bool(enabled)
}
