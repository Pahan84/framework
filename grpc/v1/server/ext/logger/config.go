package fgrpclogger

import (
	fconfig "gitlab.com/Pahan84/framework/config"
	fgrpc "gitlab.com/Pahan84/framework/grpc/v1/server"
)

const (
	root    = fgrpc.ExtRoot + ".logger"
	enabled = root + ".enabled"
)

func init() {
	fconfig.Add(enabled, true, "enable/disable logger")
}

func IsEnabled() bool {
	return fconfig.Bool(enabled)
}
