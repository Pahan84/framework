package fgrpc

import "gitlab.com/Pahan84/framework/config"

const (
	root                 = "f.grpc"
	port                 = root + ".port"
	maxConcurrentStreams = root + ".maxConcurrentStreams"
	tlsEnabled           = root + ".tls.enabled"
	certFile             = root + ".tls.certFile"
	keyFile              = root + ".tls.keyFile"
	caFile               = root + ".tls.CAFile"
	ExtRoot              = root + ".ext"
)

func init() {

	fconfig.Add(port, 9090, "server grpc port")
	fconfig.Add(maxConcurrentStreams, 5000, "server grpc max concurrent streams")
	fconfig.Add(tlsEnabled, false, "Use TLS - required for HTTP2.")
	fconfig.Add(certFile, "./cert/out/localhost.crt", "Path to the CRT/PEM file.")
	fconfig.Add(keyFile, "./cert/out/localhost.key", "Path to the private key file.")
	fconfig.Add(caFile, "./cert/out/blackbox.crt", "Path to the certificate authority (CA).")

}
