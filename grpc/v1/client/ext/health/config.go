package fgrpchealth

import (
	"gitlab.com/Pahan84/framework/config"
	"gitlab.com/Pahan84/framework/grpc/v1/client"
)

const (
	root        = fgrpc.ExtRoot + ".health"
	name        = root + ".name"
	description = root + ".description"
	required    = root + ".required"
	enabled     = root + ".enabled"
)

func init() {

	fconfig.Add(name, "grpc", "health name")
	fconfig.Add(description, "default connection", "define health description")
	fconfig.Add(required, true, "define health description")
	fconfig.Add(enabled, true, "enable/disable health")
}
