package fgrpchealth

import (
	"context"

	"google.golang.org/grpc"

	"gitlab.com/Pahan84/framework/health"
	"gitlab.com/Pahan84/framework/log"
)

type Integrator struct {
	options *Options
}

func NewIntegrator(options *Options) *Integrator {
	return &Integrator{options: options}
}

func NewDefaultIntegrator() *Integrator {
	o, err := DefaultOptions()
	if err != nil {
		flog.Fatalf(err.Error())
	}

	return NewIntegrator(o)
}

func (i *Integrator) Register(ctx context.Context, conn *grpc.ClientConn) error {

	logger := flog.FromContext(ctx).WithTypeOf(*i)

	logger.Trace("integrating grpc with health")

	checker := NewChecker(conn)
	hc := fhealth.NewHealthChecker(i.options.Name, i.options.Description, checker, i.options.Required, i.options.Enabled)
	fhealth.Add(hc)

	logger.Debug("grpc integrated on health with success")

	return nil
}
