package fgrpcprometheus

import (
	fconfig "gitlab.com/Pahan84/framework/config"
	fgrpc "gitlab.com/Pahan84/framework/grpc/v1/client"
)

const (
	root    = fgrpc.ExtRoot + ".prometheus"
	enabled = root + ".enabled"
)

func init() {
	fconfig.Add(enabled, true, "enable/disable prometheus")
}

func IsEnabled() bool {
	return fconfig.Bool(enabled)
}
