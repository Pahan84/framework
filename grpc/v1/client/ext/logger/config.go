package fgrpclogger

import (
	"gitlab.com/Pahan84/framework/config"
	"gitlab.com/Pahan84/framework/grpc/v1/client"
)

const (
	root    = fgrpc.ExtRoot + ".logger"
	enabled = root + ".enabled"
)

func init() {
	fconfig.Add(enabled, true, "enable/disable logger")
}

func IsEnabled() bool {
	return fconfig.Bool(enabled)
}
