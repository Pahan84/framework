package ffasthttp

import (
	"time"

	fconfig "gitlab.com/Pahan84/framework/config"
)

const (
	root                          = "f.fasthttp.client"
	name                          = root + ".name"
	noDefaultUserAgentHeader      = root + ".noDefaultUserAgentHeader"
	maxConnsPerHost               = root + ".maxConnsPerHost"
	maxConnWaitTimeout            = root + ".maxConnWaitTimeout"
	readBufferSize                = root + ".readBufferSize"
	writeBufferSize               = root + ".writeBufferSize"
	readTimeout                   = root + ".readTimeout"
	writeTimeout                  = root + ".writeTimeout"
	maxIdleConnDuration           = root + ".maxIdleConnDuration"
	disableHeaderNamesNormalizing = root + ".disableHeaderNamesNormalizing"
	dialDualStack                 = root + ".dialDualStack"
	maxResponseBodySize           = root + ".maxResponseBodySize"
	maxIdemponentCallAttempts     = root + ".maxIdemponentCallAttempts"
)

func init() {

	fconfig.Add(name, "", "used in User-Agent request header")
	fconfig.Add(noDefaultUserAgentHeader, false, "User-Agent header to be excluded from the Request")
	fconfig.Add(maxConnsPerHost, 512, "the maximum number of concurrent connections")
	fconfig.Add(readBufferSize, 0, "per-connection buffer size for responses' reading")
	fconfig.Add(writeBufferSize, 0, "per-connection buffer size for requests' writing")
	fconfig.Add(maxConnWaitTimeout, 0*time.Second, "maximum amount of time to wait for a connection to be free")
	fconfig.Add(readTimeout, 0*time.Second, "maximum duration for full response reading (including body)")
	fconfig.Add(writeTimeout, 0*time.Second, "maximum duration for full request writing (including body)")
	fconfig.Add(maxIdleConnDuration, 10*time.Second, "the default duration before idle keep-alive")
	fconfig.Add(disableHeaderNamesNormalizing, false, "header names are passed as-is without normalization")
	fconfig.Add(dialDualStack, "", "attempt to connect to both ipv4 and ipv6 addresses if set to true")
	fconfig.Add(maxResponseBodySize, 52428800, "maximum response body size")
	fconfig.Add(maxIdemponentCallAttempts, 5, "maximum number of attempts for idempotent calls")
}
