package fredishealth

import (
	"context"

	"github.com/go-redis/redis/v8"

	fhealth "gitlab.com/Pahan84/framework/health"
	flog "gitlab.com/Pahan84/framework/log"
)

type ClusterIntegrator struct {
	options *Options
}

func NewClusterIntegrate(options *Options) *ClusterIntegrator {
	return &ClusterIntegrator{options: options}
}

func NewDefaultClusterIntegrator() *ClusterIntegrator {
	o, err := DefaultOptions()
	if err != nil {
		flog.Fatalf(err.Error())
	}

	return NewClusterIntegrate(o)
}

func (i *ClusterIntegrator) Register(ctx context.Context, client *redis.ClusterClient) error {

	logger := flog.FromContext(ctx).WithTypeOf(*i)

	logger.Trace("integrating redis with health")

	checker := NewClusterClientChecker(client)
	hc := fhealth.NewHealthChecker(i.options.Name, i.options.Description, checker, i.options.Required, i.options.Enabled)
	fhealth.Add(hc)

	logger.Debug("redis integrated on health with success")

	return nil
}
