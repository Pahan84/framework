package fredishealth

import (
	fconfig "gitlab.com/Pahan84/framework/config"
	fredis "gitlab.com/Pahan84/framework/redis/v8"
)

const (
	root        = fredis.ExtRoot + ".health"
	name        = root + ".name"
	description = root + ".description"
	required    = root + ".required"
	enabled     = root + ".enabled"
)

func init() {

	fconfig.Add(name, "redis", "health name")
	fconfig.Add(description, "default connection", "define health description")
	fconfig.Add(required, true, "define health description")
	fconfig.Add(enabled, true, "enable/disable health")
}
