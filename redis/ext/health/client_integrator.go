package fredishealth

import (
	"context"

	"github.com/go-redis/redis/v8"

	fhealth "gitlab.com/Pahan84/framework/health"
	flog "gitlab.com/Pahan84/framework/log"
)

type ClientIntegrator struct {
	options *Options
}

func NewClientIntegrator(options *Options) *ClientIntegrator {
	return &ClientIntegrator{options: options}
}

func NewDefaultClientIntegrator() *ClientIntegrator {
	o, err := DefaultOptions()
	if err != nil {
		flog.Fatalf(err.Error())
	}

	return NewClientIntegrator(o)
}

func (i *ClientIntegrator) Register(ctx context.Context, client *redis.Client) error {

	logger := flog.FromContext(ctx).WithTypeOf(*i)

	logger.Trace("integrating redis with health")

	checker := NewClientChecker(client)
	hc := fhealth.NewHealthChecker(i.options.Name, i.options.Description, checker, i.options.Required, i.options.Enabled)
	fhealth.Add(hc)

	logger.Debug("redis integrated on health with success")

	return nil
}
