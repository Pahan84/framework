module gitlab.com/Pahan84/framework

go 1.16

require (
	github.com/ansrivas/fiberprometheus/v2 v2.1.2
	github.com/aws/aws-sdk-go v1.34.28 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/go-playground/validator/v10 v10.6.1
	github.com/go-redis/redis/v8 v8.11.5
	github.com/gobeam/stringy v0.0.4
	github.com/gobuffalo/genny v0.1.1 // indirect
	github.com/gobuffalo/gogen v0.1.1 // indirect
	github.com/gofiber/fiber/v2 v2.12.0
	github.com/golang/protobuf v1.5.2
	github.com/golang/snappy v0.0.4 // indirect
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/jackc/pgx/v4 v4.11.0
	github.com/karrick/godirwalk v1.10.3 // indirect
	github.com/klauspost/compress v1.16.5 // indirect
	github.com/knadh/koanf v1.1.0
	github.com/lann/builder v0.0.0-20180802200727-47ae307949d0
	github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0 // indirect
	github.com/montanaflynn/stats v0.7.0 // indirect
	github.com/rs/zerolog v1.23.0
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/pflag v1.0.5
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/testify v1.7.0
	github.com/valyala/fasthttp v1.26.0
	github.com/xdg-go/scram v1.1.2 // indirect
	github.com/youmark/pkcs8 v0.0.0-20201027041543-1326539a0a0a // indirect
	go.mongodb.org/mongo-driver v1.11.6
	go.opentelemetry.io/otel v0.20.0 // indirect
	golang.org/x/crypto v0.8.0 // indirect
	golang.org/x/sync v0.2.0 // indirect
	google.golang.org/grpc v1.38.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
