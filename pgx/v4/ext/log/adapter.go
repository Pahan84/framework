package fpgxlog

import (
	"context"

	"github.com/jackc/pgx/v4"

	flog "gitlab.com/Pahan84/framework/log"
)

type Logger struct {
	logger flog.Logger
}

// NewLogger accepts a zerolog.Logger as input and returns a new custom pgx
// logging fascade as output.
func NewLogger(logger flog.Logger) *Logger {
	return &Logger{
		logger: logger.WithField("module", "pgx"),
	}
}

func (pl *Logger) Log(ctx context.Context, level pgx.LogLevel, msg string, data map[string]interface{}) {
	pgxlog := pl.logger.WithFields(data)
	switch level {
	case pgx.LogLevelError:
		pgxlog.Error(msg)
	case pgx.LogLevelWarn:
		pgxlog.Warn(msg)
	case pgx.LogLevelInfo, pgx.LogLevelNone:
		pgxlog.Info(msg)
	case pgx.LogLevelDebug, pgx.LogLevelTrace:
		pgxlog.Debug(msg)
	default:
		pgxlog.Debug(msg)
	}
}
