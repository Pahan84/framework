package fpgxlog

import (
	"context"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	flog "gitlab.com/Pahan84/framework/log"
)

func Register(ctx context.Context, conn *pgxpool.Pool) error {

	if !IsEnabled() {
		return nil
	}

	logger := flog.FromContext(ctx)

	logger.Trace("integrating pgx with logger")

	conn.Config().ConnConfig.LogLevel = pgx.LogLevelInfo
	conn.Config().ConnConfig.Logger = NewLogger(logger)
	if IsDebug() {
		conn.Config().ConnConfig.LogLevel = pgx.LogLevelDebug
	}

	logger.Debug("logger integrated with pgx with success")

	return nil
}
