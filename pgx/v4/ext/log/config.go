package fpgxlog

import (
	"gitlab.com/Pahan84/framework/config"
	"gitlab.com/Pahan84/framework/pgx/v4"
)

const (
	root    = fpgx.ExtRoot + ".logger"
	enabled = root + ".enabled"
	debug   = root + ".debug"
)

func init() {
	fconfig.Add(enabled, true, "enable/disable pgx logger")
	fconfig.Add(debug, false, "enable/disable debug level")
}

func IsEnabled() bool {
	return fconfig.Bool(enabled)
}

func IsDebug() bool {
	return fconfig.Bool(debug)
}
