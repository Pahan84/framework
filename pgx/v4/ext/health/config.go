package fpgxhealth

import (
	"gitlab.com/Pahan84/framework/config"
	"gitlab.com/Pahan84/framework/pgx/v4"
)

const (
	root        = fpgx.ExtRoot + ".health"
	name        = root + ".name"
	description = root + ".description"
	required    = root + ".required"
	enabled     = root + ".enabled"
)

func init() {
	fconfig.Add(name, "pgx", "health name")
	fconfig.Add(description, "default connection", "define health description")
	fconfig.Add(required, true, "define health description")
	fconfig.Add(enabled, true, "enable/disable health")
}
