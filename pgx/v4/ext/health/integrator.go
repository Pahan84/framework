package fpgxhealth

import (
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/Pahan84/framework/health"
	"gitlab.com/Pahan84/framework/log"
)

type Integrator struct {
	options *Options
}

func NewIntegrator(options *Options) *Integrator {
	return &Integrator{options: options}
}

func NewDefaultIntegrator() *Integrator {
	o, err := DefaultOptions()
	if err != nil {
		flog.Fatalf(err.Error())
	}

	return NewIntegrator(o)
}

func (i *Integrator) Integrate(client *pgxpool.Pool) error {

	logger := flog.WithTypeOf(*i)

	logger.Trace("integrating mongo with health")

	checker := NewChecker(client)
	hc := fhealth.NewHealthChecker(i.options.Name, i.options.Description, checker, i.options.Required, i.options.Enabled)
	fhealth.Add(hc)

	logger.Debug("mongo integrated on health with success")

	return nil
}
