package fpgxhealth

import (
	"context"

	"github.com/jackc/pgx/v4/pgxpool"
)

type Checker struct {
	client *pgxpool.Pool
}

func (c *Checker) Check(ctx context.Context) error {
	return c.client.QueryRow(context.Background(), "SELECT 1+1").Scan(new(int))
}

func NewChecker(client *pgxpool.Pool) *Checker {
	return &Checker{client: client}
}
