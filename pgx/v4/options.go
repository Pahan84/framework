package fpgx

import (
	"crypto/tls"
	"fmt"
	"time"

	"github.com/lann/builder"

	fconfig "gitlab.com/Pahan84/framework/config"
)

// Options is the common structure used as the options for the repositories.
type Options struct {
	URI                string
	Host               string
	Port               uint16
	Database           string
	Username           string
	Password           string
	Debug              bool
	LogLevel           string
	MaxConn            int32
	MaxConnLifetime    time.Duration
	MaxConnIdleTime    time.Duration
	TLSEnable          bool
	InsecureSkipVerify bool
	CertFile           string
	KeyFile            string
	CAFile             string
	tlsConfig          *tls.Config
}
type optionsBuilder builder.Builder

func (b optionsBuilder) URI(value string) optionsBuilder {
	return builder.Set(b, "URI", value).(optionsBuilder)
}

func (b optionsBuilder) Host(value string) optionsBuilder {
	return builder.Set(b, "Host", value).(optionsBuilder)
}

func (b optionsBuilder) Port(value uint16) optionsBuilder {
	if value == 0 {
		value = 5432
	}
	return builder.Set(b, "Port", value).(optionsBuilder)
}

func (b optionsBuilder) Database(value string) optionsBuilder {
	return builder.Set(b, "Database", value).(optionsBuilder)
}

func (b optionsBuilder) Username(value string) optionsBuilder {
	return builder.Set(b, "Username", value).(optionsBuilder)
}

func (b optionsBuilder) Password(value string) optionsBuilder {
	return builder.Set(b, "Password", value).(optionsBuilder)
}

func (b optionsBuilder) Debug(value bool) optionsBuilder {
	return builder.Set(b, "Debug", value).(optionsBuilder)
}

func (b optionsBuilder) LogLevel(value string) optionsBuilder {
	return builder.Set(b, "LogLevel", value).(optionsBuilder)
}

func (b optionsBuilder) MaxConns(value int32) optionsBuilder {
	return builder.Set(b, "MaxConn", value).(optionsBuilder)
}

func (b optionsBuilder) MaxConnLifetime(value time.Duration) optionsBuilder {
	return builder.Set(b, "MaxConnLifetime", value).(optionsBuilder)
}

func (b optionsBuilder) MaxConnIdleTime(value time.Duration) optionsBuilder {
	return builder.Set(b, "MaxConnIdleTime", value).(optionsBuilder)
}

func (b optionsBuilder) TlsEnable(value bool) optionsBuilder {
	return builder.Set(b, "Enable", value).(optionsBuilder)
}

func (b optionsBuilder) InsecureSkipVerify(value bool) optionsBuilder {
	return builder.Set(b, "InsecureSkipVerify", value).(optionsBuilder)
}

func (b optionsBuilder) CertFile(value string) optionsBuilder {
	return builder.Set(b, "certFile", value).(optionsBuilder)
}

func (b optionsBuilder) KeyFile(value string) optionsBuilder {
	return builder.Set(b, "keyFile", value).(optionsBuilder)
}

func (b optionsBuilder) CAFile(value string) optionsBuilder {
	return builder.Set(b, "caFile", value).(optionsBuilder)
}

func (b optionsBuilder) Build() Options {
	return builder.GetStruct(b).(Options)
}

var OptionsBuilder = builder.Register(optionsBuilder{}, Options{}).(optionsBuilder)

func DefaultOptions() (*Options, error) {
	o := &Options{}

	err := fconfig.UnmarshalWithPath(root, o)
	if err != nil {
		return nil, err
	}
	fmt.Printf("%v", o)
	return o, nil
}
