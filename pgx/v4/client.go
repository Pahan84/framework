package fpgx

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	flog "gitlab.com/Pahan84/framework/log"
)

type Ext func(context.Context, *pgxpool.Pool) error

func NewClient(ctx context.Context, options *Options, exts ...Ext) (*pgxpool.Pool, error) {
	logger := flog.FromContext(ctx)
	cfg := connConfig(ctx, options)
	conn, err := newClient(ctx, cfg)
	if err != nil {
		return nil, err
	}

	for _, ext := range exts {
		if err := ext(ctx, conn); err != nil {
			panic(err)
		}
	}
	logger.Infof("Connected to AMQP server: %s", options.Host)

	return conn, nil
}

func NewDefaultClient(ctx context.Context, exts ...Ext) (*pgxpool.Pool, error) {
	l := flog.FromContext(ctx)

	o, err := DefaultOptions()
	if err != nil {
		l.Fatalf(err.Error())
	}

	return NewClient(ctx, o, exts...)
}

func newClient(ctx context.Context, pc *pgxpool.Config) (*pgxpool.Pool, error) {
	l := flog.FromContext(ctx)

	// Establish connection with provided config.
	connPool, err := pgxpool.ConnectConfig(ctx, pc)
	if err != nil {
		return nil, err
	}

	conn, err := connPool.Acquire(ctx)
	if err != nil {
		return nil, err
	}
	if err = conn.Conn().Ping(ctx); err != nil {
		return nil, err
	}

	l.Infof("Connected to Postgres server: %v", connPool.Config().ConnConfig.Host)

	return connPool, err
}

func connConfig(ctx context.Context, o *Options) *pgxpool.Config {
	var (
		cfg *pgxpool.Config
	)

	if o.TLSEnable {
		o.tlsConfig = createTlsConfig(ctx, o)
	}

	cfg, _ = pgxpool.ParseConfig(o.URI)
	if o.URI == "" {
		cfg.ConnConfig.Host = o.Host
		cfg.ConnConfig.Port = o.Port
		cfg.ConnConfig.Database = o.Database
		cfg.ConnConfig.User = o.Username
		cfg.ConnConfig.Password = o.Password
	}
	cfg.ConnConfig.TLSConfig = o.tlsConfig
	if o.Debug {
		cfg.ConnConfig.LogLevel = pgx.LogLevelDebug
	} else {
		l, err := pgx.LogLevelFromString(o.LogLevel)
		if err != nil {
			l = pgx.LogLevelError
		}
		cfg.ConnConfig.LogLevel = l
	}
	cfg.MinConns = 2
	cfg.MaxConns = o.MaxConn
	cfg.MaxConnLifetime, cfg.MaxConnIdleTime = o.MaxConnLifetime, o.MaxConnIdleTime

	return cfg
}

func createTlsConfig(ctx context.Context, o *Options) *tls.Config {
	l := flog.FromContext(ctx)

	// Load the client certificates from disk
	cert, err := tls.LoadX509KeyPair(o.CertFile, o.KeyFile)
	if err != nil {
		l.Fatalf("could not load client key pair: %s", err)
	}
	if o.CAFile != "" {
		// Create a certificate pool from the certificate authority
		certPool := x509.NewCertPool()
		ca, err := ioutil.ReadFile(o.CAFile)
		if err != nil {
			l.Fatalf("could not read ca certificate: %s", err)
		}

		// Append the certificates from the CA
		if ok := certPool.AppendCertsFromPEM(ca); !ok {
			l.Fatalf("failed to append ca certs")
		}

		return &tls.Config{
			ServerName:         o.Host, // NOTE: this is required!
			Certificates:       []tls.Certificate{cert},
			RootCAs:            certPool,
			InsecureSkipVerify: o.InsecureSkipVerify,
		}

	}

	return &tls.Config{
		ServerName:         o.Host, // NOTE: this is required!
		Certificates:       []tls.Certificate{cert},
		InsecureSkipVerify: o.InsecureSkipVerify,
	}
}
