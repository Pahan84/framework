package fpgx

import (
	"time"

	fconfig "gitlab.com/Pahan84/framework/config"
)

const (
	root            = "f.pgx"
	Uri             = root + ".uri"
	Host            = root + ".host"
	Port            = root + ".port"
	Database        = root + ".database"
	Username        = root + ".username"
	Password        = root + ".password"
	Debug           = root + ".debug"
	logLevel        = root + ".loglevel"
	maxConn         = root + ".maxConn"
	maxConnLifetime = root + ".maxConnLifetime"
	maxConnIdleTime = root + ".maxConnIdleTime"
	tlsEnabled      = root + ".tls.enabled"
	certFile        = root + ".tls.certFile"
	keyFile         = root + ".tls.keyFile"
	caFile          = root + ".tls.caFile"
	ExtRoot         = root + ".Ext"
)

func init() {

	fconfig.Add(Uri, "", "URI")
	fconfig.Add(Host, "localhost", "host")
	fconfig.Add(Port, 5432, "port")
	fconfig.Add(Database, "postgres", "define database")
	fconfig.Add(Username, "postgres", "define username")
	fconfig.Add(Password, "postgres", "define password")
	fconfig.Add(Debug, false, "define debug mod")
	fconfig.Add(logLevel, "info", "define log level")
	fconfig.Add(maxConn, 1024*3, "the maximum number of concurrent connections")
	fconfig.Add(maxConnLifetime, 2*time.Minute, "maximum amount of time ")
	fconfig.Add(maxConnIdleTime, 1*time.Minute, "maximum amount of time ")
	fconfig.Add(tlsEnabled, false, "Use TLS")
	fconfig.Add(certFile, "./cert/out/localhost.crt", "Path to the CRT/PEM file.")
	fconfig.Add(keyFile, "./cert/out/localhost.key", "Path to the private key file.")
	fconfig.Add(caFile, "./cert/out/blackbox.crt", "Path to the certificate authority (CA).")

}
