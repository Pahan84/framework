package ffiber

import (
	"context"

	"github.com/gofiber/fiber/v2"

	"gitlab.com/Pahan84/framework/rest/response"
)

func NewHealthHandler() *HealthHandler {
	return &HealthHandler{}
}

type HealthHandler struct {
}

func (u *HealthHandler) Get(c *fiber.Ctx) error {

	ctx, cancel := context.WithCancel(c.Context())
	defer cancel()

	resp, httpCode := response.NewHealth(ctx)

	return c.Status(httpCode).JSON(resp)
}
