package ffiber

import (
	"github.com/gofiber/fiber/v2"

	"gitlab.com/Pahan84/framework/rest/response"
)

func NewResourceStatusHandler() *ResourceStatusHandler {
	return &ResourceStatusHandler{}
}

type ResourceStatusHandler struct {
}

func (u *ResourceStatusHandler) Get(c *fiber.Ctx) error {
	return c.JSON(response.NewResourceStatus())
}
