package ffiberrecover

import (
	"gitlab.com/Pahan84/framework/config"
	"gitlab.com/Pahan84/framework/fiber/v2"
)

const (
	enabled = ffiber.ExtRoot + ".recover.enabled"
)

func init() {
	fconfig.Add(enabled, true, "enable/disable recover middleware")
}

func IsEnabled() bool {
	return fconfig.Bool(enabled)
}
