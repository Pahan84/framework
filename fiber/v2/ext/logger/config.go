package ffiberlogger

import (
	"gitlab.com/Pahan84/framework/config"
	"gitlab.com/Pahan84/framework/fiber/v2"
)

const (
	enabled = ffiber.ExtRoot + ".logger.enabled"
)

func init() {
	fconfig.Add(enabled, true, "enable/disable logger middleware")
}

func IsEnabled() bool {
	return fconfig.Bool(enabled)
}
