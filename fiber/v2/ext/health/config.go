package ffiberhealth

import (
	"gitlab.com/Pahan84/framework/config"
	"gitlab.com/Pahan84/framework/fiber/v2"
)

const (
	root    = ffiber.ExtRoot + ".health"
	enabled = root + ".enabled"
	route   = root + ".route"
)

func init() {
	fconfig.Add(enabled, true, "enable/disable health route")
	fconfig.Add(route, "/health", "define status url")
}

func IsEnabled() bool {
	return fconfig.Bool(enabled)
}

func getRoute() string {
	return fconfig.String(route)
}
