package ffiberhealth

import (
	"context"

	"github.com/gofiber/fiber/v2"

	flog "gitlab.com/Pahan84/framework/log"
	"gitlab.com/Pahan84/framework/rest/response"
)

func Register(ctx context.Context, app *fiber.App) error {
	if !IsEnabled() {
		return nil
	}

	logger := flog.FromContext(ctx)

	healthRoute := getRoute()

	logger.Infof("configuring health router on %s", healthRoute)

	app.Get(healthRoute, func(c *fiber.Ctx) error {

		ctx, cancel := context.WithCancel(c.Context())
		defer cancel()

		resp, httpCode := response.NewHealth(ctx)

		return c.Status(httpCode).JSON(resp)
	})

	return nil
}
