package ffibertid

import (
	"gitlab.com/Pahan84/framework/config"
	"gitlab.com/Pahan84/framework/fiber/v2"
)

const (
	root    = ffiber.ExtRoot + ".tid"
	enabled = root + ".enabled"
)

func init() {
	fconfig.Add(enabled, true, "enable/disable tid middleware")
}

func IsEnabled() bool {
	return fconfig.Bool(enabled)
}
