package ffibertid

import (
	"context"

	"github.com/gofiber/fiber/v2"
	uuid "github.com/satori/go.uuid"

	"gitlab.com/Pahan84/framework/info"
)

func Register(ctx context.Context, app *fiber.App) error {

	if !IsEnabled() {
		return nil
	}

	app.Use(middleware())

	return nil
}

func middleware() fiber.Handler {

	// Return new handler
	return func(c *fiber.Ctx) error {

		tid := c.Get("X-TID", info.AppName+"-"+uuid.NewV4().String())

		c.Context().SetUserValue("tid", tid)
		c.Append("X-TID", tid)

		// Continue stack
		return c.Next()
	}
}
