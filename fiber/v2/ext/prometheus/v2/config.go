package ffiberprometheus

import (
	"gitlab.com/Pahan84/framework/config"
	"gitlab.com/Pahan84/framework/fiber/v2"
)

const (
	ConfigRoot = ffiber.ExtRoot + ".prometheus"
	enabled    = ConfigRoot + ".enabled"
	route      = ConfigRoot + ".route"
)

func init() {
	fconfig.Add(enabled, true, "enable/disable prometheus integration")
	fconfig.Add(route, "/metrics", "define prometheus metrics url")
}

func IsEnabled() bool {
	return fconfig.Bool(enabled)
}

func getRoute() string {
	return fconfig.String(route)
}
