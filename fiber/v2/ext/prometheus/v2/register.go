package ffiberprometheus

import (
	"context"

	"github.com/ansrivas/fiberprometheus/v2"
	"github.com/gofiber/fiber/v2"

	flog "gitlab.com/Pahan84/framework/log"
)

func Register(ctx context.Context, instance *fiber.App) error {

	if !IsEnabled() {
		return nil
	}

	logger := flog.FromContext(ctx)

	logger.Trace("integrating fiber with prometheus")

	prometheusRoute := getRoute()

	logger.Infof("configuring prometheus metrics router on %s", prometheusRoute)

	prometheus := fiberprometheus.New("")
	prometheus.RegisterAt(instance, prometheusRoute)

	instance.Use(prometheus.Middleware)

	logger.Debug("prometheus integrated with fiber with success")

	return nil
}
