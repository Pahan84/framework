package ffibercors

import (
	"net/http"

	"github.com/gofiber/fiber/v2"

	"gitlab.com/Pahan84/framework/config"
	"gitlab.com/Pahan84/framework/fiber/v2"
)

const (
	root             = ffiber.ExtRoot + ".cors"
	enabled          = root + ".enabled"
	allowOrigins     = root + ".allow.origins"
	allowHeaders     = root + ".allow.headers"
	allowMethods     = root + ".allow.methods"
	allowCredentials = root + ".allow.credentials"
	exposeHeaders    = root + ".expose.headers"
	maxAge           = root + ".maxAge"
)

func init() {
	fconfig.Add(enabled, true, "enable/disable cors middleware")
	fconfig.Add(allowOrigins, []string{"*"}, "cors allow origins")
	fconfig.Add(allowHeaders, []string{fiber.HeaderOrigin, fiber.HeaderContentType, fiber.HeaderAccept},
		"cors allow headers")
	fconfig.Add(allowMethods,
		[]string{http.MethodGet, http.MethodHead, http.MethodPut, http.MethodPatch, http.MethodPost, http.MethodDelete},
		"cors allow methods")
	fconfig.Add(allowCredentials, true, "cors allow credentials")
	fconfig.Add(exposeHeaders, []string{}, "cors expose headers")
	fconfig.Add(maxAge, 5200, "cors max age (seconds)")
}

func IsEnabled() bool {
	return fconfig.Bool(enabled)
}

func getAllowOrigins() []string {
	return fconfig.Strings(allowOrigins)
}

func getAllowMethods() []string {
	return fconfig.Strings(allowMethods)
}

func getAllowHeaders() []string {
	return fconfig.Strings(allowHeaders)
}

func getAllowCredentials() bool {
	return fconfig.Bool(allowCredentials)
}

func getExposeHeaders() []string {
	return fconfig.Strings(exposeHeaders)
}

func getMaxAge() int {
	return fconfig.Int(maxAge)
}
