package ffiberstatus

import (
	"context"

	"github.com/gofiber/fiber/v2"

	"gitlab.com/Pahan84/framework/log"
	"gitlab.com/Pahan84/framework/rest/response"
)

func Register(ctx context.Context, app *fiber.App) error {
	if !IsEnabled() {
		return nil
	}

	logger := flog.FromContext(ctx)

	statusRoute := getRoute()

	logger.Infof("configuring status router on %s", statusRoute)

	app.Get(statusRoute, func(c *fiber.Ctx) error {
		return c.JSON(response.NewResourceStatus())
	})

	return nil
}
