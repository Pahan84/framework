package ffiberstatus

import (
	"gitlab.com/Pahan84/framework/config"
	"gitlab.com/Pahan84/framework/fiber/v2"
)

const (
	root    = ffiber.ExtRoot + ".status"
	enabled = root + ".enabled"
	route   = root + ".route"
)

func init() {
	fconfig.Add(enabled, true, "enable/disable status route")
	fconfig.Add(route, "/resource-status", "define status url")
}

func IsEnabled() bool {
	return fconfig.Bool(enabled)
}

func getRoute() string {
	return fconfig.String(route)
}
