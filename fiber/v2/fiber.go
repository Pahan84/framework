package ffiber

import (
	"context"
	"strconv"

	"github.com/gofiber/fiber/v2"

	flog "gitlab.com/Pahan84/framework/log"
)

var (
	app *fiber.App
)

type Ext func(context.Context, *fiber.App) error

func Start(ctx context.Context, exts ...Ext) *fiber.App {

	config, _ := AppConfig()
	app = fiber.New(*config)

	for _, ext := range exts {
		if err := ext(ctx, app); err != nil {
			panic(err)
		}
	}

	return app
}

func Serve(ctx context.Context) {
	l := flog.FromContext(ctx)
	l.Infof("starting fiber server. https://gofiber.io/")

	if getTLSEnabled() {
		l.Fatal(app.ListenTLS(serverPort(), getCertFile(), getKeyFile()))
	} else {
		l.Fatal(app.Listen(serverPort()))
	}
}

func serverPort() string {
	return ":" + strconv.Itoa(Port())
}
